WAIVER: Workshop Use
===

* Status: Draft
* Last updated date: 2019-12-29
* Last updated member: Robert Layton

## Requirement
All members must sign this waiver to use equipment at the workshop. 
Not having signed the up-to-date waiver means they cannot use any equipment, even if their membership is otherwise up to date

## Display
This waiver is displayed when a user creates an account on TidyHQ.

### Note
This has to be HTML, as TidyHQ doesn't seem to take markdown

## Text:

<p>WAIVER RELEASE &amp; INDEMNIFICATION</p>

<p>In consideration of the acceptance of my membership or participation in events with the Ballarat Hackerspace.</p>

<ol>
	<li>I waive all claims that I may have against the Ballarat Hackerspace Inc, its officers, directors, members, volunteers, employees, agents and sponsors, or its executors, administrators, heirs, successors or assigns (the organisers), and release them from all claims for death, injury or damage arising out of my participation in their club, their event and its related activities, together with any costs, including legal fees.</li>
	<li>I agree to indemnify and keep indemnified the organisers against damage arising out of my participation in the events and its related activities.</li>
	<li>I agree to comply with all the rules, regulations and instructions of the organisers.</li>
	<li>In the event of injury or illness during participation, I consent to receive medical treatment, which may be deemed advisable by the organisers.</li>
	<li>I acknowledge that I have sole responsibility for my personal possessions and equipment during all the events and activities.</li>
	<li>I understand that my membership or entry entry fee is non-transferable and not refundable.</li>
	<li>I agree to have my personal details recorded and used by the Ballarat Hackerspace and related parties for further communications of future related events.</li>
</ol>

