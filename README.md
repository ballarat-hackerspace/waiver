# waiver

Text for any waivers and agreements that people have to sign. 

Any document in here should have a header about its use, where it is displayed, and its status (i.e. draft or not)


For a template, see the file: workshop_waiver.txt


